import requests
import json

class RelationMenu:

    def __init__(self):
        self.base_url = 'http://127.0.0.1:8000/api'

    def start(self):
        stop = False
        while not stop:
            print('a -> add relation')
            print('b -> go back to previous menu')
            print()

            ch = input()
            if ch == 'b':
                stop = True
            else:
                self.print_books(self.get_all_books())
                book_id = input('Give Book Id: ')
                self.print_authors(self.get_all_authors())
                author_id = input('Give Author Id')
                self.add_relation(book_id, author_id)

    def add_relation(self, book_id, author_id):
        r = requests.post(url=self.base_url + '/book/' + book_id + '/author/' + author_id)

    def get_all_books(self):
        r = requests.get(url=self.base_url + '/books')
        data = r.json()
        return data

    def get_all_authors(self):
        r = requests.get(url=self.base_url + '/authors')
        data = r.json()
        return data

    def print_books(self, books):
        print()
        print('ID|TITLE|YEAR')
        print('---------------')

        for book in books:
            print(str(book['id']) + " " + book['title'] + " " + str(book['year']))
        print()

    def print_authors(self, authors):
        print()
        print('ID|NAME|AGE')
        print('---------')

        for author in authors:
            print(str(author['id']) + " " + author['name'] + " " + str(author['age']))
        print()