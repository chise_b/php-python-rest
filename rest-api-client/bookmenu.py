import requests
import json

class BookMenu:

    def __init__(self):
        self.base_url = 'http://127.0.0.1:8000/api'

    def start(self):
        stop = False
        while not stop:
            self.print_menu()

            ch = input()
            if ch == '0':
                self.print_books(self.get_all_books())
            elif ch == '1':
                self.add_book()
            elif ch == '2':
                self.update_book()
            elif ch == '3':
                self.delete_book()
            elif ch == '4':
                self.filter_book()
            elif ch == 'b':
                stop = True
            else:
                print('Invalid input...')

    def get_all_books(self):
        r = requests.get(url=self.base_url + '/books')
        data = r.json()
        return data

    def add_book(self):
        title = input('Give Book Title: ')
        year = int(input('Give Book year: '))

        data = {'title': title,
                'year': year}

        # sending post request and saving response as response object
        r = requests.post(url=self.base_url + '/book', data=json.dumps(data))

        books = self.get_all_books()
        self.print_books(books)

    def update_book(self):
        self.print_books(self.get_all_books())
        id = input('Give Book Id: ')
        title = input('Give new Title: ')
        year = input('Give new age: ')

        if not title.isalpha():
            title = None

        if year.isdecimal():
            year = int(year)
        else:
            year = None

        # data to be sent to api
        data = {'title': title,
                'year': year}
        r = requests.put(url=self.base_url + '/book/' + id, data=json.dumps(data))

        self.print_books(self.get_all_books())

    def delete_book(self):
        self.print_books(self.get_all_books())
        id = input('Give Book Id: ')

        r = requests.delete(url=self.base_url + '/book/' + id)

        self.print_books(self.get_all_books())

    def filter_book(self):
        filter_input = input('Filter by title: ')

        r = requests.get(url=self.base_url + '/books/filtered/' + filter_input)
        books = r.json()

        self.print_books(books)

    def print_books(self, books):
        print()
        print('ID|TITLE|YEAR')
        print('---------------')

        for book in books:
            print(str(book['id']) + " " + book['title'] + " " + str(book['year']))
        print()

    def print_menu(self):
        print()
        print('0. Get All Books')
        print('1. Add Book')
        print('2. Update Book')
        print('3. Delete Book')
        print('4. Filter Book')
        print('b->To get back to previous menu ')
        print()
