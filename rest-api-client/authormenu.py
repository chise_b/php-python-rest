import json
import requests


class AuthorMenu:

    def __init__(self):
        self.base_url = 'http://127.0.0.1:8000/api'

    def start(self):
        stop = False
        while not stop:
            self.print_menu()

            ch = input()
            if ch == '0':
                self.print_authors(self.get_all_authors())
            elif ch == '1':
                self.add_author()
            elif ch == '2':
                self.update_author()
            elif ch == '3':
                self.delete_author()
            elif ch == '4':
                self.filter_author()
            elif ch == 'b':
                stop = True
            else:
                print('Invalid input...')

    def get_all_authors(self):
        r = requests.get(url=self.base_url + '/authors')
        data = r.json()
        return data

    def update_author(self):
        self.print_authors(self.get_all_authors())
        id = input('Give author id: ')
        name = input('Give new name: ')
        age = input('Give new age: ')

        if not name.isalpha():
            name = None

        if age.isdecimal():
            age = int(age)
        else:
            age = None

        # data to be sent to api
        data = {'name': name,
                'age': age}
        r = requests.put(url=self.base_url + '/author/' + id, data=json.dumps(data))

        self.print_authors(self.get_all_authors())

    def delete_author(self):
        self.print_authors(self.get_all_authors())
        id = input('Give author id: ')

        r = requests.delete(url=self.base_url + '/author/' + id)

        self.print_authors(self.get_all_authors())

    def filter_author(self):
        filter_input = input('Filter by name: ')

        r = requests.get(url=self.base_url + '/authors/filtered/' + filter_input)
        authors = r.json()

        self.print_authors(authors)

    def add_author(self):
        name = input('Give Author name: ')
        age = int(input('Give Author age: '))

        # data to be sent to api
        data = {'name': name,
                'age': age}

        # sending post request and saving response as response object
        r = requests.post(url=self.base_url + '/author', data=json.dumps(data), headers={"Content-Type":"application/json"})

        authors = self.get_all_authors()
        self.print_authors(authors)

    def print_authors(self, authors):
        print()
        print('ID|NAME|AGE')
        print('---------')

        for author in authors:
            print(str(author['id']) + " " + author['name'] + " " + str(author['age']))
        print()

    def print_menu(self):
        print()
        print('0. Get All Authors')
        print('1. Add Author')
        print('2. Update Author')
        print('3. Delete Author')
        print('4. Filter Author')
        print('b->To get back to previous menu ')
