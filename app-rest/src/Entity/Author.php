<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="author")
 */
class Author
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     *
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     *
     */
    private $age;

    /**
     * @var \App\Entity\Book[]
     *
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="authors")
     */
    private $books;


    public function __construct()
    {
        $this->books = new ArrayCollection();
    }


    public function addBook(Book $book){
        if ($this->books->contains($book)){
            return;
        }

        $this->books->add($book);
        $book->addAuthor($this);
    }

    public function removeBook(Book $book){
        if ($this->books->contains($book)){
            return;
        }
        $this->books->removeElement($book);
        $book->removeAuthor($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'age' => $this->getAge()
        ];
    }


}