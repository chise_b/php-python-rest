<?php


namespace App\Controller;


use App\Repository\AuthorRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Author controller.
 * @Route("/api", name="api_")
 */
class AuthorController
{
    private $authorRepository;

    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * @Route("/author", name="add_author", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $name = $data['name'];
        $age = $data['age'];

        if (empty($name) || empty($age)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $this->authorRepository->saveAuthor($name, $age);

        return new JsonResponse(['status' => 'Author created!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/author/{id}", name="get_one_author", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {
        $author = $this->authorRepository->findOneBy(['id' => $id]);
        $data = [
            'id' => $author->getId(),
            'name' => $author->getName(),
            'age' => $author->getAge(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/authors", name="get_all_authors", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $authors = $this->authorRepository->findAll();
        $data = [];

        foreach ($authors as $author) {
            $data[] = [
                'id' => $author->getId(),
                'name' => $author->getName(),
                'age' => $author->getAge(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/author/{id}", name="update_author", methods={"PUT"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request): JsonResponse
    {
        $author = $this->authorRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        empty($data['name']) ? true : $author->setName($data['name']);
        empty($data['age']) ? true : $author->setAge($data['age']);

        $updatedAuthor = $this->authorRepository->updateAuthor($author);

        return new JsonResponse($updatedAuthor->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/author/{id}", name="delete_author", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $author = $this->authorRepository->findOneBy(['id' => $id]);

        $this->authorRepository->removeAuthor($author);

        return new JsonResponse(['status' => 'Author deleted'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/authors/filtered/{filter}", name="get_all_filtered_authors", methods={"GET"})
     */
    public function getAllFilteredAuthors($filter): JsonResponse
    {
        $authors = $this->authorRepository->findAll();
        $data = [];

        foreach ($authors as $author) {
            if (strpos($author->getName(), $filter) !== false){
                $data[] = [
                    'id' => $author->getId(),
                    'name' => $author->getName(),
                    'age' => $author->getAge(),
                ];
            }
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

}