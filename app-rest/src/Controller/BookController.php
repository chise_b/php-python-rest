<?php


namespace App\Controller;

use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Book controller.
 * @Route("/api", name="api_")
 */
class BookController
{
    private $bookRepository;
    private $authorRepository;

    public function __construct(BookRepository $bookRepository, AuthorRepository $authorRepository)
    {
        $this->bookRepository = $bookRepository;
        $this->authorRepository = $authorRepository;
    }

    /**
     * @Route("/book", name="add_book", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $title = $data['title'];
        $year = $data['year'];

        if (empty($title) || empty($year)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $this->bookRepository->saveBook($title, $year);

        return new JsonResponse(['status' => 'Book created!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/book/{id}", name="get_one_book", methods={"GET"})
     * @param $id
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {
        $author = $this->bookRepository->findOneBy(['id' => $id]);
        $data = [
            'id' => $author->getId(),
            'title' => $author->getTitle(),
            'year' => $author->getYear(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/books", name="get_all_books", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $authors = $this->bookRepository->findAll();
        $data = [];

        foreach ($authors as $author) {
            $data[] = [
                'id' => $author->getId(),
                'title' => $author->getTitle(),
                'year' => $author->getYear(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/book/{id}", name="update_book", methods={"PUT"})
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request): JsonResponse
    {
        $book = $this->bookRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        empty($data['title']) ? true : $book->setTitle($data['title']);
        empty($data['year']) ? true : $book->setYear($data['year']);

        $updatedBook = $this->bookRepository->updateBook($book);

        return new JsonResponse($updatedBook->toArray(), Response::HTTP_OK);
    }

    /**
     * @Route("/book/{id}", name="delete_book", methods={"DELETE"})
     */
    public function delete($id): JsonResponse
    {
        $book = $this->bookRepository->findOneBy(['id' => $id]);

        $this->bookRepository->removeBook($book);

        return new JsonResponse(['status' => 'Book deleted'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/book/{bookId}/author/{authorId}", name="add_author_to_book", methods={"POST"})
     */
    public function addAuthorToBook($bookId, $authorId)
    {
        $book = $this->bookRepository->findOneBy(['id' => $bookId]);
        $author = $this->authorRepository->findOneBy(['id' => $authorId]);

        $book->addAuthor($author);
        $updatedBook = $this->bookRepository->updateBook($book);
        return new JsonResponse(['status' => 'Author added to book'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/books/filtered/{filter}", name="get_all_filtered_books", methods={"GET"})
     */
    public function getAllFilteredBooks($filter): JsonResponse
    {
        $authors = $this->bookRepository->findAll();
        $data = [];

        foreach ($authors as $author) {
            if (strpos($author->getTitle(), $filter) !== false){
                $data[] = [
                    'id' => $author->getId(),
                    'title' => $author->getTitle(),
                    'year' => $author->getYear(),
                ];
            }
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }
}