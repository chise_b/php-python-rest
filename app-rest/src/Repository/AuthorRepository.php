<?php


namespace App\Repository;


use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class AuthorRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct
    (
        ManagerRegistry $registry,
        EntityManagerInterface $manager
    )
    {
        parent::__construct($registry, Author::class);
        $this->manager = $manager;
    }

    public function updateAuthor(Author $author): Author
    {
        $this->manager->persist($author);
        $this->manager->flush();

        return $author;
    }

    public function saveAuthor($name, $age)
    {
        $newAuthor = new Author();

        $newAuthor
            ->setName($name);
        $newAuthor
            ->setAge($age);

        $this->manager->persist($newAuthor);
        $this->manager->flush();
    }

    public function removeAuthor(Author $author)
    {
        $this->manager->remove($author);
        $this->manager->flush();
    }
}