<?php


namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class BookRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct
    (
        ManagerRegistry $registry,
        EntityManagerInterface $manager
    )
    {
        parent::__construct($registry, Book::class);
        $this->manager = $manager;
    }

    public function updateBook(Book $book): Book
    {
        $this->manager->persist($book);
        $this->manager->flush();

        return $book;
    }

    public function saveBook($title, $year)
    {
        $newBook = new Book();

        $newBook
            ->setTitle($title);
        $newBook
            ->setYear($year);

        $this->manager->persist($newBook);
        $this->manager->flush();
    }

    public function removeBook(Book $book)
    {
        $this->manager->remove($book);
        $this->manager->flush();
    }
}