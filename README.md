# PHP Python REST

### Problem statement
    
    This project was done as part of the class of Web Services and Middleware Technologies while 
    pursuing my master degree in Distributed Systems.
    
    The project represents a console-based simple implementation of a library.
    Features:
        * CRUD on books
        * CRUD on authors
        * filter books after title parts
        * filter authors after name parts 
        
### Technical details
     This project consists of a REST implementation in PHP using Symfony 5 and a client that consumes those REST APIs in Python.
     
     How to create database schema:
        php bin/console doctrine:schema:create
     
     How to start server:
        (inside app-rest) symfony server:start
        
     How to start client:
        python3 main.py
     
     As ORM was used doctrine and as DB - mysql.
     Prerequisites: 
        * python 3.x
        * PHP 7.x
        * a db called phprest, 
        * symfony 5
    
### Documentation and useful links
    
    1. Symfony installation guide - https://symfony.com/download
    2. Doctrine many-to-many relation code - https://gist.github.com/Ocramius/3121916
    3. Assoctiation mapping - https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/reference/association-mapping.html
    
